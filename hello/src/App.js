import React from 'react';
import './App.css';
import Exam1, { Exam, Exam2 } from './hello';

import Header from './components/header/header'

function App() {
  return (
    <div className="App">
      <Header />
      <Exam />
      <Exam2 />
    </div>
  );
}

export default App;
